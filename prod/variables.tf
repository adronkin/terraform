variable "project" {
  description = "Project ID"
}

variable "region" {
  description = "Region"
  default     = "europe-west4"
}

variable "zone" {
  description = "Zone"
  default     = "europe-west4-a"
}

variable "app_count" {
  description = "Count app instances"
  default     = 1
}

variable "app_disk_image" {
  description = "Disk image for app"
  default     = "app-base"
}

variable "db_disk_image" {
  description = "Disk image for Mongo"
  default     = "mongo-base"
}

variable "username" {
  description = "Username"
}

variable "public_key_path" {
  description = "Path to the public key for ssh access"
}

variable "private_key_path" {
  description = "Path to the private key for ssh access"
}

variable "ssh_rule_name" {
  description = "Name for the rule"
  default     = "default-allow-ssh"
}

variable "source_ranges" {
  description = "Allowed IP addresses"
  default     = ["0.0.0.0/0"]
}

variable "db_instance_name" {
  description = "DB instance name"
}

variable "app_instance_name" {
  description = "App instance name"
}

variable "ip_resource_name" {
  description = "Name for google_compute_address"
}

variable "app_firewall_name" {
  description = "Name for google_compute_firewall"
}

variable "db_firewall_name" {
  default = "Name for google_compute_firewall"
}
