terraform {
  backend "gcs" {
    bucket  = "app-production-storage"
    prefix  = "prod"
  }
}
