terraform {
  required_version = ">= 0.13"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.82.0"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.region
}

module "app" {
  source            = "../modules/app"
  app_instance_name = "${var.app_instance_name}-${count.index + 1}"
  zone              = var.zone
  app_disk_image    = var.app_disk_image
  username          = var.username
  private_key_path  = var.private_key_path
  public_key_path   = var.public_key_path
  ip_resource_name  = "${var.ip_resource_name}-${count.index + 1}"
  count             = var.app_count
}

module "db" {
  source           = "../modules/db"
  db_instance_name = var.db_instance_name
  zone             = var.zone
  db_disk_image    = var.db_disk_image
  username         = var.username
  public_key_path  = var.public_key_path
  db_firewall_name = var.db_firewall_name
}

module "vpc" {
  source            = "../modules/vpc"
  ssh_rule_name     = var.ssh_rule_name
  app_firewall_name = var.app_firewall_name
  username          = var.username
  public_key_path   = var.public_key_path
  source_ranges     = var.source_ranges
}
