variable "project" {
  description = "Project ID"
}

variable "region" {
  description = "Region"
  default     = "europe-west4"
}

variable "name" {
  description = "Storage name"
}

variable "stage" {
  description = "Stage name"
}

variable "namespace" {
  description = "Namespace name"
}
