terraform {
  required_version = ">= 0.13"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.82.0"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.region
}

module "storage-bucket" {
  enabled       = true
  source        = "SweetOps/storage-bucket/google"
  name          = var.name
  stage         = var.stage
  namespace     = var.namespace
  version       = "0.4.0"
  location      = var.region
  force_destroy = true
}

output "storage-bucket-url" {
  value = module.storage-bucket.url
}

output "storage-bucket-link" {
  value = module.storage-bucket.self_link
}
