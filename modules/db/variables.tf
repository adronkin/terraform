variable "zone" {
  description = "Zone"
  default     = "europe-west4-a"
}

variable "db_disk_image" {
  description = "Disk image for Mongo"
  default     = "mongo-base"
}

variable "username" {
  description = "Username"
}

variable "public_key_path" {
  description = "Path to the public key for ssh access"
}

variable "db_instance_name" {
  description = "DB instance name"
}

variable "db_firewall_name" {
  default = "Name for google_compute_firewall"
}
