resource "google_compute_instance" "db" {
  name         = var.db_instance_name
  machine_type = "e2-micro"
  zone         = var.zone
  tags         = ["reddit-db"]

  boot_disk {
    initialize_params {
      image = var.db_disk_image
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }

  metadata = {
    ssh-keys = "${var.username}:${file("${var.public_key_path}")}"
  }
}

resource "google_compute_firewall" "firewall-mongo" {
  name          = var.db_firewall_name
  network       = "default"
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["reddit-db"]
  source_tags   = ["reddit-app"]

  allow {
    protocol = "tcp"
    ports    = ["27017"]
  }
}
