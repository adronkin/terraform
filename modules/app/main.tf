resource "google_compute_instance" "reddit" {
  name         = var.app_instance_name
  machine_type = "e2-micro"
  zone         = var.zone
  tags         = ["reddit-app"]

  boot_disk {
    initialize_params {
      image = var.app_disk_image
    }
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.app_ip.address
    }
  }

  connection {
    type        = "ssh"
    user        = var.username
    agent       = false
    private_key = file(var.private_key_path)
    host        = self.network_interface.0.access_config.0.nat_ip
    timeout     = "60s"
  }

  provisioner "file" {
    source      = "../modules/app/files/puma.service"
    destination = "/tmp/puma.service"
  }

  provisioner "remote-exec" {
    script = "../modules/app/files/deploy.sh"
  }

  metadata = {
    ssh-keys = "${var.username}:${file("${var.public_key_path}")}"
  }
}

resource "google_compute_address" "app_ip" {
  name = var.ip_resource_name
}
