#!/bin/bash
set -e

sudo git clone -b monolith https://github.com/express42/reddit.git /opt/reddit/
cd /opt/reddit/ && sudo bundle install

sudo mv /tmp/puma.service /etc/systemd/system/puma.service
sudo systemctl start puma
sudo systemctl enable puma
