variable "zone" {
  description = "Zone"
  default     = "europe-west4-a"
}

variable "app_disk_image" {
  description = "Disk image for app"
  default     = "app-base"
}

variable "username" {
  description = "Username"
}

variable "public_key_path" {
  description = "Path to the public key for ssh access"
}

variable "private_key_path" {
  description = "Path to the private key for ssh access"
}

variable "app_instance_name" {
  description = "App instance name"
}

variable "ip_resource_name" {
  description = "Name for google_compute_address"
}
