resource "google_compute_firewall" "firewall_ssh" {
  name          = var.ssh_rule_name
  network       = "default"
  source_ranges = var.source_ranges
  description   = "Allow SSH from anywhere"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

resource "google_compute_firewall" "firewall-puma" {
  name          = var.app_firewall_name
  network       = "default"
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["reddit-app"]

  allow {
    protocol = "tcp"
    ports    = ["9292"]
  }
}

resource "google_compute_project_metadata" "add_ssh_key" {
  metadata = {
    ssh-keys = "${var.username}:${file("${var.public_key_path}")}"
  }
}
