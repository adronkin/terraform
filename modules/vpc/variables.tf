variable "username" {
  description = "Username"
}

variable "public_key_path" {
  description = "Path to the public key for ssh access"
}

variable "source_ranges" {
  description = "Allowed IP addresses"
  default     = ["0.0.0.0/0"]
}

variable "ssh_rule_name" {
  description = "Name for the rule"
  default     = "default-allow-ssh"
}

variable "app_firewall_name" {
  description = "Name for google_compute_firewall"
}
